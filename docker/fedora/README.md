# Fedora30 i386

It was not possible to incluse the base image for Fedora 30 i386.
Unfortunately it is too big (~150MB) and gitlab did not accept it (max 100MB).

Anyway we publish the docker images, which should be enough.

`gitlab-registry.cern.ch/cernvm/build-images/fedora_i386:base30`
