FROM        gitlab-registry.cern.ch/cernvm/build-images/slc_i386:base6

RUN         yum -y update
RUN         yum -y install yum-plugin-ovl
RUN         yum -y install                     \
                       cmake                   \
                       curl-devel              \
                       fuse-devel              \
                       fuse3-devel             \
                       gcc                     \
                       gcc-c++                 \
                       git                     \
                       golang                  \
                       hardlink                \
                       libattr-devel           \
                       libcap-devel            \
                       libuuid-devel           \
                       make                    \
                       openssl-devel           \
                       policycoreutils-python  \
                       python-devel            \
                       python-setuptools       \
                       rpm-build               \
                       selinux-policy-devel    \
                       selinux-policy-targeted \
                       sysvinit-tools          \
                       valgrind-devel          \
                       voms-devel              \
                       which                   \
                       zlib-devel

RUN go get github.com/jstemmer/go-junit-report

USER        sftnight
WORKDIR     /home/sftnight

