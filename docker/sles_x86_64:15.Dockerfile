FROM       alpine:latest AS builder
MAINTAINER Jakob Blomer <jblomer@cern.ch>

RUN mkdir -p /root/sles
WORKDIR /root/sles

RUN apk --no-cache add ca-certificates curl
RUN curl https://ecsft.cern.ch/dist/cvmfs/builddeps/sl15.x86_64.tar.gz | tar xvfz -
RUN rm -rf /root/sles/sys /root/sles/dev /root/sles/proc

FROM scratch
ARG SFTNIGHT_UID=5005
ARG SFTNIGHT_GID=5005

WORKDIR /
COPY --from=builder /root/sles /

RUN groupadd -g $SFTNIGHT_GID sftnight && \
  useradd -u $SFTNIGHT_UID -g sftnight sftnight

USER       sftnight
WORKDIR    /home/sftnight
