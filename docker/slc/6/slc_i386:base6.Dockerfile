FROM        scratch

ARG SFTNIGHT_UID=5005
ARG SFTNIGHT_GID=5005

ADD         slc6_i386.tar.gz /

RUN  groupadd --gid $SFTNIGHT_GID sftnight && \
        adduser --uid $SFTNIGHT_UID --gid $SFTNIGHT_GID sftnight

