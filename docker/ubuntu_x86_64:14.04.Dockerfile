FROM        amd64/ubuntu:14.04
MAINTAINER  Jakob Blomer <jblomer@cern.ch>

ARG SFTNIGHT_UID=5005
ARG SFTNIGHT_GID=5005

RUN         apt-get -y update && apt-get -y upgrade
RUN         apt-get -y update && apt-get -y install                        \
                                              autotools-dev                \
                                              cmake                        \
                                              cpio                         \
                                              debhelper                    \
                                              devscripts                   \
                                              gdb                          \
                                              git                          \
                                              libattr1-dev                 \
                                              libcap-dev                   \
                                              libfuse-dev                  \
                                              libssl-dev                   \
                                              pkg-config                   \
                                              python-dev                   \
                                              python-setuptools            \
                                              python3-dev                  \
                                              python3-setuptools           \
                                              unzip                        \
                                              uuid-dev                     \
                                              valgrind

RUN  groupadd --gid $SFTNIGHT_GID sftnight && \
        adduser --uid $SFTNIGHT_UID --gid $SFTNIGHT_GID sftnight

USER        sftnight
WORKDIR     /home/sftnight
